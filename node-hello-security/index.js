const express = require('express');
const app = new express();

var http = require('http'),
    fs = require('fs');

app.use(function applyXFrame(req, res, next) {
      res.set('X-Frame-Options', 'DENY');
      next(); 
  });    

fs.readFile('./index.html', function (err, html) {
    if (err) {
        throw err; 
    }       
    http.createServer(function(request, response) {  
        response.writeHeader(200, {"Content-Type": "text/html"});  
        response.write(html);  
        response.end();  
    }).listen(3000);
});